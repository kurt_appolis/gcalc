# Gratuity Calculator #

A super simply gratuity calculator to help out when you trying to figure out your final bill.

### Fonts used ###

* Gatsby [http://www.1001freefonts.com/gatsby.font]

### Requires only ###

* Input for Total on Bill
* Optional: Perc. of the gratuity (tip) you want to give.
