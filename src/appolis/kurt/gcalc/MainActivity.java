package appolis.kurt.gcalc;

import java.text.DecimalFormat;

import android.R.color;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements TextWatcher {

	private Typeface font = null;

	private EditText input_total_amount_et, input_gratuity_amount_et = null;
	private TextView input_main_title_tv, input_total_amount_tv,
			input_gratuity_amount_tv, output_main_title_tv,
			output_gratuity_title, output_gratuity_amount,
			output_grand_total_title, output_grand_total_amount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle();
		setupView();
	}

	private void setupView() {
		font = Typeface.createFromAsset(getAssets(), "GatsbyFLF-Bold.ttf");
		registerTextViews();
		registerEditTexts();
		setFontFace();
	}

	private void setTitle() {
		getSupportActionBar().setTitle("Gratuity Calculator");

		// get image and set action bar color
		Bitmap bm = BitmapFactory.decodeResource(getResources(),
				R.drawable.backdrop);

		// Asynchronous
		Palette p = Palette.from(bm).generate();

		// set action bar's color
		getSupportActionBar()
				.setBackgroundDrawable(
						new ColorDrawable(p
								.getDarkVibrantColor(color.background_dark)));
	}

	private void registerEditTexts() {
		input_total_amount_et = (EditText) findViewById(R.id.input_total_amount_et);
		input_gratuity_amount_et = (EditText) findViewById(R.id.input_gratuity_amount_et);

		input_total_amount_et.addTextChangedListener(this);
		input_gratuity_amount_et.addTextChangedListener(this);
	}

	private void registerTextViews() {
		input_main_title_tv = (TextView) findViewById(R.id.input_main_title_tv);

		input_total_amount_tv = (TextView) findViewById(R.id.input_total_amount_tv);
		input_gratuity_amount_tv = (TextView) findViewById(R.id.input_gratuity_amount_tv);
		output_main_title_tv = (TextView) findViewById(R.id.output_main_title_tv);

		output_gratuity_title = (TextView) findViewById(R.id.output_gratuity_title);
		output_gratuity_amount = (TextView) findViewById(R.id.output_gratuity_amount);
		output_grand_total_title = (TextView) findViewById(R.id.output_grand_total_title);
		output_grand_total_amount = (TextView) findViewById(R.id.output_grand_total_amount);

	}

	private void setFontFace() {
		// EditText
		input_total_amount_et.setTypeface(font);
		input_gratuity_amount_et.setTypeface(font);

		// TextView
		input_main_title_tv.setTypeface(font);

		input_total_amount_tv.setTypeface(font);
		input_gratuity_amount_tv.setTypeface(font);
		output_main_title_tv.setTypeface(font);

		output_gratuity_title.setTypeface(font);
		output_gratuity_amount.setTypeface(font);
		output_grand_total_title.setTypeface(font);
		output_grand_total_amount.setTypeface(font);

	}

	double RoundTo2Decimals(double val) {
		DecimalFormat df2 = new DecimalFormat("###.##");
		return Double.valueOf(df2.format(val));
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		double total_amount = 0;
		double gratuity_perc = 0;
		try {
			gratuity_perc = Double.parseDouble(input_gratuity_amount_et
					.getText().toString());
			total_amount = Double.parseDouble(input_total_amount_et.getText()
					.toString());

			double gratuity_amount = RoundTo2Decimals(total_amount
					* (gratuity_perc / 100));

			output_gratuity_amount.setText("R " + gratuity_amount);

			double grand_total = RoundTo2Decimals(total_amount
					+ gratuity_amount);

			output_grand_total_amount.setText("R " + grand_total);
		} catch (Exception e) {
			Toast.makeText(MainActivity.this, "Error converting string",
					Toast.LENGTH_SHORT).show();
		}

	}

}
